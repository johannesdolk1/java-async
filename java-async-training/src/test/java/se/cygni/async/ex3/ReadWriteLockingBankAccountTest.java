package se.cygni.async.ex3;

import org.junit.Test;
import se.cygni.async.Exercise;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ReadWriteLockingBankAccountTest {

    @Test
    @Exercise
    public void testThreadSafe() throws Exception {
        ReadWriteLockingBankAccount account = new ReadWriteLockingBankAccount();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10000000; i++) {
            int amount = i % 2 == 0 ? 1 : -1;
            executorService.submit(() -> {
                account.deposit(amount);
            });
        }
        executorService.shutdown();
        executorService.awaitTermination(30, TimeUnit.SECONDS);
        assertEquals(0, account.getTotal());
    }

}