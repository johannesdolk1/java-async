package se.cygni.async.ex5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import se.cygni.async.Exercise;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Compare the performace with apache bench between http://localhost:8080/sync/ and http://localhost:8080/async/
 *
 * Simplify the code by creating a @RestController instead of a servlet that returns a Callable instead.
 */
@Exercise
@SpringBootApplication
@ServletComponentScan("com.cygni.async")
public class ServletApp {

    @Bean(destroyMethod = "shutdown")
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(1);
    }

    public static void main(String[] args) {
        SpringApplication.run(ServletApp.class, args);
    }
}