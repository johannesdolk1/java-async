package se.cygni.async.ex5;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.concurrent.ExecutorService;

import static org.slf4j.LoggerFactory.getLogger;

@Component("async")
public class AsyncServlet extends HttpServlet {

    private static final Logger LOGGER = getLogger(AsyncServlet.class);

    @Autowired
    ExecutorService executorService;

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        AsyncContext asyncContext = request.startAsync();
        executorService.submit(() -> doWork(asyncContext));
    }

    private void doWork(AsyncContext context) {
        try {
            ServletResponse response = context.getResponse();
            response.setContentType("text/plain");
            PrintWriter out = response.getWriter();
            out.printf("Thread %s completed the task", Thread.currentThread().getName());
            out.flush();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            context.complete();
        }
    }
}
