package se.cygni.async.ex3;

/** Make this class thread safe using {@link java.util.concurrent.locks.ReentrantLock} */
public class LockingAccount {
    private int total;

    public int getTotal() {
        return total;
    }

    public void deposit(int amount) {
        total += amount;
    }
}
