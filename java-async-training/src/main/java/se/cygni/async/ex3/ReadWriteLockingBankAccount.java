package se.cygni.async.ex3;

/** Make this class thread safe using {@link java.util.concurrent.locks.ReentrantReadWriteLock} */
public class ReadWriteLockingBankAccount {
    private int total;

    public int getTotal() {
        return total;
    }

    public void deposit(int amount) {
        total += amount;
    }
}
