package se.cygni.async.threads;

public class BankAccountSafe {

    private static class Account {
        private int total;

        public synchronized void deposit(int amount) {
            if (amount <= 0) {
                throw new IllegalArgumentException("amount < 0: " + amount);
            }
            total += amount;
        }

        @Override
        public String toString() {
            return "Account{" + "total=" + total + '}';
        }
    }

    private static class Person extends Thread {
        private final String name;
        private final Account account;

        public Person(String name, Account account) {
            this.name = name;
            this.account = account;
        }

        public void run() {
            int amount = 10000;
            for (int i = 0; i < amount; i++) {
                account.deposit(1);
            }
            System.out.println(name + " deposited: " + amount + ". " + account);
        }
    }


    public static void main(String[] args) throws InterruptedException {
        Account account = new Account();
        new Person("Jack", account).start();
        new Person("Amy", account).start();
        Thread.sleep(1000);
        System.out.println(account);
    }
}
