package se.cygni.async.threads;

public class NoVisibilitySafe {
    private static Object LOCK = new Object();
    private static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread {
        public void run() {
            while (true) {
                synchronized (LOCK) {
                    if (ready) {
                        System.out.println(number);
                        return;
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new ReaderThread().start();
        synchronized (LOCK) {
            number = 42;
            ready = true;
        }
    }
}
