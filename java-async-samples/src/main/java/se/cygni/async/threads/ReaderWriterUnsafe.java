package se.cygni.async.threads;

public class ReaderWriterUnsafe {
    public static boolean ready;
    public static int number;

    private static class ReaderThread extends Thread {

        public void run() {
            System.out.println("Reader started...");
            while (true) {
                while (!ready)
                    Thread.yield();
                int n = number;
                if (n != 42) {
                    System.out.println("Reader expected 42 got: " + n);
                }
                number = 0;
                ready = false;
            }
        }
    }

    private static class WriterThread extends Thread {

        public void run() {
            int counter = 0;
            System.out.println("Writer started...");
            while (true) {
                while (ready)
                    Thread.yield();
                int n = number;
                if (n != 0) {
                    System.out.println("Writer expect 0 got: " + n);
                }
                // Try to uncomment the line below and see what happens
                // counter++;
                number = 42;
                ready = true;
            }
        }
    }

    public static void main(String[] args) {
        new ReaderThread().start();
        new WriterThread().start();
    }
}
