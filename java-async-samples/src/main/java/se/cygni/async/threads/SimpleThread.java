package se.cygni.async.threads;

import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

public class SimpleThread {
    private static final Logger LOGGER = getLogger(SimpleThread.class);

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            LOGGER.info("Starting work...");
            doWork();
            LOGGER.info("done.");
        });
        LOGGER.info("Starting thread.");
        t.start();

        LOGGER.info("Waiting for thread to die...");
        t.join();
        LOGGER.info("Thread is dead.");
    }

    private static void doWork() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }


}
