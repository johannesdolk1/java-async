package se.cygni.async.threads;

import java.util.Arrays;
import java.util.Comparator;

@SuppressWarnings("ALL")
public class BankAccountTransferSafe {
    private static class Account {
        private static int count = 0;
        private int id;
        private final String name;
        private int total;

        private Account(int id, String name, int total) {
            this.id = id;
            this.name = name;
            this.total = total;
        }

        public static synchronized Account create(String name, int total) {
            return new Account(++count, name, total);
        }

        public synchronized void deposit(int amount) {
            requirePositive(amount);
            total += amount;
        }

        public synchronized int withdraw(int amount) {
            requirePositive(amount);
            amount = Math.min(amount, total - amount);
            total -= amount;
            return amount;
        }

        public int transfer(Account to, int amount) {
            Account lock1;
            Account lock2;
            if (this.id < to.id) {
                lock1 = this;
                lock2 = to;
            } else {
                lock1 = to;
                lock2 = this;
            }
            synchronized (lock1) {
                synchronized (lock2) {
                    amount = withdraw(amount);
                    to.deposit(amount);
                }
            }
            return amount;
        }

        private void requirePositive(int amount) {
            if (amount <= 0) {
                throw new IllegalArgumentException("amount < 0: " + amount);
            }
        }

        @Override
        public synchronized String toString() {
            return name + ": " + total;
        }
    }


    private static class Transfer extends Thread {

        private final Account from;
        private final Account to;
        private final int amount;

        public Transfer(Account from, Account to, int amount) {
            this.from = from;
            this.to = to;
            this.amount = amount;
        }

        public void run() {
            from.transfer(to, amount);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Account sallaryAccount = Account.create("sallery", 1000);
        Account savingsAccount = Account.create("savings", 1000);
        for (int i = 0; i < 1000; i++) {
            new Transfer(sallaryAccount, savingsAccount, 200).start();
            new Transfer(savingsAccount, sallaryAccount, 200).start();
        }
        System.out.println(sallaryAccount);
        System.out.println(savingsAccount);
    }

}