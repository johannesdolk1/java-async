package se.cygni.async;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@SuppressWarnings({"unused", "UnusedAssignment"})
public class SyncAsync {


    private Result syncCall(String data) {
        return null;
    }

    private class Result {
    }


    public void calls() throws ExecutionException, InterruptedException {
        String data = null;
        // Call fails or returns the result
        Result result = syncCall(data);

        // Asynch: Returns a reference where to get result.
        // can fail too.
        ResultRef resultRef = asyncCall(data);
        result = getResult(resultRef); // Check if result is ready. Can fail.
        while (result == null) {
            Thread.sleep(1000);
            result = getResult(resultRef);
        }

        // This is similar to async await in JS 7.
        Future<Result> futureResult = asyncCallFuture(data);
        result = futureResult.get(); // Block or get result or fail
    }

    private Result getResult(ResultRef resultRef) {
        return null;
    }

    private ResultRef asyncCall(String data) {
        return null;
    }

    private Future<Result> asyncCallFuture(String data) {
        return null;
    }


    private class ResultRef {
    }
}
