
package se.cygni.async.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    private static final String SIMPLE_QUEUE = "simple.queue";
    
    private final JmsTemplate jmsTemplate;
    
    @Autowired
    public ClientService(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }
    
    public void addOrder(Order order) {
        jmsTemplate.convertAndSend(SIMPLE_QUEUE, order);
    }
}